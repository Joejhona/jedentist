# APP SUDENTIS 2.0

Aplicacion para la gestion de clinicas odontologicas en linea.

## Para Empezar 

La aplicacion funciona con DJANGO, entonces tienes que instalar DJANGO te recomiendo que uses un entorno virtual de CONDA.

### Prerequisitos

* [conda](https://www.djangoproject.com/) - Para crear entornos virtuales
* [Django](https://www.djangoproject.com/) - El Framework 

### Instrucciones de instalacion 

Realiza los siguientes pasos y busca en la documentacion de cada aplicacion

1. Instalar miniconda en windows o linux, en cada link estan las instrucciones.

    * [miniconda windows](https://conda.io/projects/conda/en/latest/user-guide/install/windows.html)
    * [miniconda linux](https://conda.io/projects/conda/en/latest/user-guide/install/linux.html)

    **AVISO** La instalación de miniconda es mas rapida que anaconda.
    
2. Crear un entorno virual e ingresa al entorno virtual, esto creara un entorno virtual donde estara python, darle si a todo.


    En windows
    ```
    "Abre la terminal prompt de miniconda o conda que esta en el boton de inicio" 
    (conda) C:\xxxx>
    (conda) C:\xxxx>conda create -n joe-django python
    proceed ([y]/n)? y
    instalando
    (conda) C:\xxxx>conda activate joe-django
    (joe-django) C:\xxxx>
    (joe-django) C:\xxxx>python -V
    Python 3.7.2
    ```

    En Linux
    ```
    xxxx$ conda create -n joe-django python
    proceed ([y]/n)? y
    instalando
    xxxx$ source activate joe-django
    (joe-django) xxxx$
    (joe-django) xxxx$python -V
    Python 3.7.2
    ```
    Si deseas puedes crear o eliminar más entornos, busca las instrucciones en la documentación  

3. Instalar DJANGO y comprobar la version

    En windows
    ```
    (joe-django) C:\xxxx> pip install Django
    esperar que instale
    (joe-django) C:\xxxx> python
    Python 3.7.2 (default, Dec 29 2018, 06:19:36)
    [GCC 7.3.0] :: Anaconda, Inc. on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import django
    >>> print(django.get_version())
    2.1.5
    >>> exit()
    (joe-django) C:\xxxx> 
    ```

    En Linux
    ```
    (joe-django) xxxx$ pip install Django
    "esperar que instale y verificar la instalaciones"
    (joe-django) xxxx$ python -m django --version
    2.1.5
    (joe-django) xxxx$
    ```

## Corriendo por primera vez la aplicación 

Seguir los siguientes pasos:

1. Descarga la aplicacion, descomprime y dirijete a la carpeta jedentist en la terminal 

    * Download zip (para windows)
    * Download tar (para linux)
    
    El boton de Download esta en la parte superior derecha de la pagina y tiene el simbolo de la nube (debajo del boton azul clone).

![Captura_de_pantalla_de_2019-01-31_08_07_16](/uploads/55cc5948e9efc85e4b62e3a9281db769/Captura_de_pantalla_de_2019-01-31_08_07_16.png)
    
    ```
    "Dirigiendote a la aplicacion"
    C:xxx> cd Descargas/jedentist-master/
    C:xxx/Descargas/jedentist-master> dir 
    * assets
    * db.sqlite3
    * jedentist
    * manage
    * README
    * sudentis
    * templates
    ```


2. Correr la migración de la base de datos, en windos o linux es igual:

```
si saliste del entorno virtual joe-django tienes que volver a entrar
(joe-django) xxx:~/Descargas/jedentist-master$ python manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions, sudentis
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying sessions.0001_initial... OK
  Applying sudentis.0001_initial... OK
(joe-django) xxx:~/Descargas/jedentist-master$
```
3. Correr y entrar a la aplicacion Sudentis 2.0

```
(joe-django) xxx:~/Descargas/jedentist-master$ python manage.py runserver
Performing system checks...

System check identified no issues (0 silenced).
January 30, 2019 - 11:16:37
Django version 2.1.5, using settings 'jedentist.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

[ENTRAR A APPSUDENTIS 2.0](http://127.0.0.1:8000/)

![appsudentis](/uploads/e73723cb96cc9d38a56a1f83dc81188a/appsudentis.png)

## Creando Usuarios para la aplicacion

**AVISO** No cerrar la terminal prompt donde se esta ejecutando la aplicacion, esto detendra la aplicación. Si cerraste el terminal debes hacer los siguientes pasos:
* Ingresa a la terminal miniconda en el boton de inicio
* Ingresa al entorno virtual joe-django --->> conda activate joe-django
* Dirigete a la carpeta jedentist-master
* Corre la aplicacion con --->> python manage.py runserver


La aplicacion recien esta en fase de construcción asi que no te asustes ni te molestes si falla, las funciones basicas que puede hacer son las siguientes:

* Crear un usuario dentista, quien vendria ser el dentista dueño de una clinica o dentista trabajador de una clinica.
* Crear un usuario paciente, quien seria el ciudadano comun que se atiende en las clinicas odontologicas.
* Crear una historia clinica, donde estan registradas todas las atenciones del paciente. 
* Crear una clinica odontologica. 

1. Creando un dentista

    * En la pagina de inicio registrate como dentista o proveedor colocando la informacion que te pide y click en Proovedor o Dentista.
    * En la siguiente pagina debes completar tu Nombre y DNI.
    * Entraste a tu sesion de consultior personal donde: 
        * En la parte superior de la pagina se tiene un avatar de usuario con tu nombre, numero de colegiatura y DNI.
        * Lista de historias clinicas creadas por ti.
        * Lista de pacientes atendidos.
    * En la parte izquierda aparecera la opcion de crear mi consultorio.

2. Creando consultorio o clinica dental

    * Click a la opcion de crear mi consultorio.
    * Colocar el Nombre y RUC de tu clinica.
    * Click en crear clinica.
    * Entraste a la pagina de tu clinica donde:
        * En la parte superior aparece el nombre de tu clinica y tu estado que vendria a ser de administrador 
        * Puedes agregar dentistas que vendrian a ser los trabajadores de tu clinica.
        * Buscar o crear una historia clinica, que seria para realizar tus atenciones.

3. Creando una historia clinica
    * Coloca el dni, celular o cualquier otro numero del paciente como historia clinica.
    * Click en la cruz verde.
    * Completar los datos del paciente y siguiente.
    * Agregar el tratamiento y guardar la historia.
    * Agregar la atencion y costo en la ventana de color verde.
    * Se pueden agregar varias atenciones.

Algunas opciones estan funcionando, es cuestion de ir descubriendo poco a poco la aplicacion. Dejen sus preguntas y comentarios.

## Autor

joejhona@gmail.com