from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
        ]
        labels = {
            'username':'Colegiatura o RUC',
            'first_name':'Nombres',
            'last_name':'Apellidos',
            'email':'Correo',
        }
        help_texts = {
            'username':'Si eres dentista ingresa tu Colegitura si eres provedor ingresa tu RUC',
        }