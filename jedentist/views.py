from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from .forms import RegisterForm


def homepage(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            #### Agregar el validador que permite verificar la colegiatura
            # username = form.cleaned_data['username']
            # if username existe en el registro
            # and se encuentra habilitado
            # entonces se procedera a grabar
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            #### luego de grabar los datos principales, save in user: 
            # 1. first_name el nombre del doctor
            # 2. last_name el apellido
            return redirect('sudentis:dentist-create')
    else:
        form = RegisterForm()
    context = {'form':form}
    return render(request, 'homepage.html', context)


