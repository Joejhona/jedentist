from django.contrib import admin
from .models import Dentist, Clinic, History, Attention, Patient

admin.site.register(Dentist)
admin.site.register(Clinic)
admin.site.register(History)
admin.site.register(Attention)
admin.site.register(Patient)


