from django import forms
from django.forms import ModelForm, Textarea 
from .models import Attention, History, Patient

class DentistIdForm(forms.Form):
    # Agregar el validador de max y minimo aunque no creo que sea necesario
    your_id_dentist = forms.CharField(label='Agregar Dentistas',help_text='Ingresa la Colegiatura')

class AttentionForm(ModelForm):
    class Meta:
        model   = Attention
        fields  = ['attention_text','attention_price']
        widgets = {'attention_text': Textarea(attrs={'cols': 80, 'rows': 5}),}

class PatientForm(ModelForm):
    class Meta:
        model  = Patient
        fields = ['patient_dni']
        labels = {'patient_dni':'Buscar o Crear Historia',}

class HistoryForm(ModelForm):
    class Meta:
        model  = History
        fields = ['history_num']