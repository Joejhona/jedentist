import datetime
from django.utils import timezone
from django.conf import settings
from django.db import models
from django.urls import reverse

class Dentist(models.Model):
    dentist_dni     = models.PositiveIntegerField('DNI:', primary_key=True)
    dentist_name    = models.CharField('Nombre y Apellido:',max_length=70)
    dentist_create  = models.DateTimeField(default=datetime.datetime.now)
    dentist_user    = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return 'Dentista: %s' % self.dentist_name 

    def get_absolute_url(self, **kwargs): 
        return reverse('sudentis:dentist-detail', kwargs={'pk': self.dentist_dni})

class Patient(models.Model):
    patient_dni         = models.PositiveIntegerField('DNI del paciente:', primary_key=True, help_text='Colocar el DNI')
    patient_name        = models.CharField('Nombres del paciente:',max_length=40)
    patient_age         = models.PositiveIntegerField('Edad del paciente:', default=18)
    #last_name   = models.CharField('Apellidos:',max_length=100)

    def __str__(self):
        return 'Paciente: %s' % (self.patient_name)

    def get_absolute_url(self, **kwargs): 
        #return reverse('sudentis:index')
        return reverse('sudentis:patient-detail', kwargs={'pk':self.patient_dni})

class Clinic(models.Model):
    clinic_name     = models.CharField('Nombre:', max_length=40, help_text='Si deseas no lo pongas')
    clinic_ruc      = models.PositiveIntegerField('RUC:', primary_key=True, help_text='Solo ingresa tu RUC y se extraeran los datos de la SUNAT para crear tu Clinica')
    clinic_user     = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=None)
    clinic_workers  = models.ManyToManyField(Dentist, default=None)
    clinic_patients = models.ManyToManyField(Patient, through='History', default=None)
    clinic_create   = models.DateTimeField(default=datetime.datetime.now)

    def get_absolute_url(self, **kwargs): 
        return reverse('sudentis:clinic-detail', kwargs={'pk': self.clinic_ruc})

class History(models.Model): 
    history_num         = models.AutoField(primary_key=True)
    history_patient     = models.ForeignKey(Patient, on_delete=models.CASCADE) 
    history_clinic      = models.ForeignKey(Clinic, on_delete=models.CASCADE) 
    history_treatment   = models.CharField('Tratamiento',max_length=500)

    def get_absolute_url(self, **kwargs): 
        return reverse('sudentis:history-detail', kwargs={'pk': self.pk})

class Attention(models.Model):
    attention_id        = models.AutoField(primary_key=True)
    attention_text      = models.CharField('Atencion:', max_length=500)
    attention_price     = models.PositiveIntegerField('Costo:')
    attention_create    = models.DateTimeField('Fecha de atencion:', default=datetime.datetime.now) 
    attention_history   = models.ForeignKey(History, on_delete=models.CASCADE) 

    def was_attend_recently(self): 
        return self.date_att >= timezone.now() - datetime.timedelta(days=1) 
