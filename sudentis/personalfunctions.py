from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission

def add_perm_personal(self, model, can_view=False, can_edit=False, can_delete=False):
    content_type    = ContentType.objects.get_for_model(model)
    usuario = self.request.user
    name    = self.request.user.username
    if can_view is True:
        codename_view   = 'can_view_only_'+name
        name_view       = 'puede ver solo '+name
        perm_view, created_view = Permission.objects.get_or_create(
            codename    = codename_view,
            name        = name_view,
            content_type=content_type,
        )
        usuario.user_permissions.add(perm_view)
    if can_edit is True:
        codename_edit   = 'can_edit_only_'+name
        name_edit       = 'puede editar solo '+name
        perm_edit, created_edit = Permission.objects.get_or_create(
            codename    = codename_edit,
            name        = name_edit,
            content_type=content_type,
        )
        usuario.user_permissions.add(perm_edit)
    if can_delete is True:
        codename_delete = 'can_delete_only_'+name
        name_delete     = 'puede borrar solo '+name
        perm_delete, created_delete = Permission.objects.get_or_create(
            codename    = codename_delete,
            name        = name_delete,
            content_type=content_type,
        )
        usuario.user_permissions.add(perm_delete)
    return