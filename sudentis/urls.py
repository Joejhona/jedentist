from django.urls import path
from .views import index, DentistCreate, DentistDetailView, ClinicCreate, ClinicDetailView, HistoryUpdate, HistoryDetailView, dentist_add_work, attention_add, PatientUpdate, patient_add, PatientDetailView

app_name = 'sudentis'

urlpatterns = [
    path('', index, name='index'), # ex: /sudentis/
    path('dentist-create/', DentistCreate.as_view(), name='dentist-create'),
    path('dentist/<int:pk>/', DentistDetailView, name='dentist-detail'),
    path('clinic-create/', ClinicCreate.as_view(), name='clinic-create'),
    path('clinic/<int:pk>/', ClinicDetailView, name='clinic-detail'),
    path('clinic/<int:pk>/dentist-add/', dentist_add_work, name='dentist-add'),
    path('clinic/<int:pk>/patient-add/', patient_add, name='patient-add'),
    path('history-update/<int:pk>/', HistoryUpdate.as_view(), name='history-update'),
    path('history/<int:pk>/', HistoryDetailView, name='history-detail'),
    path('history/<int:pk>/attention-add/', attention_add, name='attention-add'),
    path('patient-update/<int:pk>/', PatientUpdate.as_view(), name='patient-update'),
    path('patient/<int:pk>/', PatientDetailView.as_view(), name='patient-detail')
]