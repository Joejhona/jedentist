from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView, UpdateView
from .models import Dentist, Clinic, History, Attention, Patient
from .personalfunctions import add_perm_personal
from .forms import DentistIdForm, AttentionForm, HistoryForm, PatientForm
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate #, get_permission_codename 
from django.contrib.auth.models import Permission, User
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic.detail import DetailView


def index(request):
    dentist_list = Dentist.objects.order_by('-dentist_create')[:5]
    context = {'dentist_list': dentist_list}
    usuario = request.user
    holas   = usuario.get_all_permissions()#agregar el object para ver si sale algo
    #holass = get_permission_codename()
    print(holas)
    #revisa la pagina 704
    return render(request, 'sudentis/index.html', context)

def dentist_add_work(request, pk):
    if request.method == 'POST':
        form_dent   = DentistIdForm(request.POST)
        if form_dent.is_valid():
            dentist = form_dent.cleaned_data['your_id_dentist']
            try:
                user_work   = User.objects.get(username=dentist)
            except User.DoesNotExist:
                return HttpResponseRedirect(reverse('sudentis:clinic-detail', kwargs={'pk': pk}))
            dentista = user_work.dentist
            if dentista:
                content_type     = ContentType.objects.get_for_model(Dentist)
                permission       = Permission.objects.get(
                    codename     = 'can_view_only_'+request.user.username,
                    content_type = content_type,
                )
                user_work.user_permissions.add(permission)
                clinica  = Clinic.objects.get(clinic_user=request.user)
                clinica.clinic_workers.add(dentista)
                #puede redirigir a un json formato o aun aviso
            return HttpResponseRedirect(reverse('sudentis:clinic-detail', kwargs={'pk': pk}))       

def attention_add(request, pk):
    if request.method == 'POST':
        form = AttentionForm(request.POST)
        new_attention = form.save(commit=False)
        historia = get_object_or_404(History, history_num=pk)
        new_attention.attention_history = historia
        new_attention.save()
        form.save_m2m()
        return HttpResponseRedirect(reverse('sudentis:history-detail', kwargs={'pk': pk}))
        #return reverse('sudentis:history-detail', kwargs={'pk': pk, 'dni': dni})

@login_required
def patient_add(request, pk):
    if request.method == 'POST':
        form_pati = PatientForm(request.POST)
        if form_pati.is_valid():
            patient_dni = form_pati.cleaned_data['patient_dni']
            patient_new, created_patient = Patient.objects.get_or_create(patient_dni=patient_dni)
            clinica = get_object_or_404(Clinic, pk=pk)
            history_new, created_history = History.objects.get_or_create(history_patient=patient_new, 
                                                history_clinic=clinica)
            #adicionar permisos para que pueda agregar historias o pacientes, revisar
            if created_patient is True:#--> se creo el paciente
                return HttpResponseRedirect(reverse('sudentis:patient-update', kwargs={'pk':patient_dni}))
            elif created_history is True:
                return HttpResponseRedirect(reverse('sudentis:history-update', kwargs={'pk':patient_dni}))
            else:
                return HttpResponseRedirect(reverse('sudentis:history-detail', kwargs={'pk':patient_dni}))


@method_decorator(login_required, name='dispatch')
class DentistCreate(CreateView):
    model = Dentist
    fields = ['dentist_name','dentist_dni']    
    def form_valid(self, form):
        form.instance.dentist_user = self.request.user
        add_perm_personal(self,Dentist,can_view=True,can_edit=True)
        return super().form_valid(form)

@login_required
def DentistDetailView(request, pk):
    usuario = request.user
    dentista= get_object_or_404(Dentist, pk=pk)
    if usuario.has_perm('sudentis.can_view_only_'+dentista.dentist_user.username):
        context = {'dentista':dentista}
        return render(request, 'sudentis/dentist_detail.html', context)
    else:
        return redirect('sudentis:index')

@method_decorator(login_required, name='dispatch')
class ClinicCreate(CreateView):
    model = Clinic
    fields = ['clinic_name','clinic_ruc']    
    def form_valid(self, form):
        form.instance.clinic_user = self.request.user
        add_perm_personal(self,Clinic,can_view=True,can_edit=True)
        return super().form_valid(form)

@login_required
def ClinicDetailView(request, pk):
    usuario   = request.user
    clinica   = get_object_or_404(Clinic, pk=pk)
    historias = History.objects.filter(history_clinic=clinica)
    form_dent = DentistIdForm()
    form_pati = PatientForm()
    if usuario.has_perm('sudentis.can_view_only_'+clinica.clinic_user.username):
        context = {'clinica':clinica,'form_dent':form_dent,'form_pati':form_pati,'historias':historias,}
        return render(request, 'sudentis/clinic_detail.html', context)
    else:
        return redirect('sudentis:index')

@login_required
def HistoryDetailView(request, pk):
    usuario    = request.user
    historia   = get_object_or_404(History, pk=pk)
    atenciones = Attention.objects.filter(attention_history=historia)
    form       = AttentionForm()
    if usuario.has_perm('sudentis.can_view_only_'+historia.history_clinic.clinic_user.username):
        context = {'historia':historia, 'form':form, 'atenciones':atenciones}
        return render(request, 'sudentis/history_detail.html', context)
    else:
        return redirect('sudentis:index')

"""
class HistoryDetailView(DetailView):
    model = History
"""
@method_decorator(login_required, name='dispatch')
class PatientUpdate(UpdateView):
    model  = Patient
    fields = ['patient_name','patient_age','patient_dni']
    template_name_suffix = '_update_form'
    def get_success_url(self):
        #agrgar el condicinal y redirigir a la historia creada 
        if self.request.user.clinic.clinic_user:
            historia   = get_object_or_404(History, history_patient=self.object, history_clinic=self.request.user.clinic)
        elif self.request.user.dentist.clinic.clinic_workers:
            historia   = get_object_or_404(History, history_patient=self.object, history_clinic=self.request.user.dentist.clinic)
        return reverse('sudentis:history-update', kwargs={'pk': historia.pk})

class PatientDetailView(DetailView):
    model = Patient

@method_decorator(login_required, name='dispatch')
class HistoryUpdate(UpdateView):
    model  = History
    fields = ['history_treatment']
    template_name_suffix = '_update_form'
    def form_valid(self, form):
        form.instance.history_clinic = self.request.user.clinic
        return super().form_valid(form)